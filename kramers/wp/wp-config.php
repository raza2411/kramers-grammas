<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'kramers');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'su?V&/kV7Q|q:CfUi%c#]nJ^wA+ ,i!m9q9{.{%r |jDQb-(ITYh(=A)v=~q{)~h');
define('SECURE_AUTH_KEY',  ';d`A_+$LL`/xQn`z*IY>WOv,D^|!no3O_g*PdYz7BL|$LFxrZg6jjJ}&/$a.G#ii');
define('LOGGED_IN_KEY',    'O1ilR)*GUf{yMguJ!yw:H~6$;/bKf`uT{/ue3%.hmx9$Xork}^9EA,sR:]Lnv6~$');
define('NONCE_KEY',        'r&R_@[Q*C9J: *Vm8#@&>|{-:R6KXDB]=2o0~R_Iai*mB)th*Gx6@k1y,DE5XV,7');
define('AUTH_SALT',        'u28yk{wu$ +Brz@y5NqKJgh#V4.p,`bD]$W?P;QR6n@jU;6K+z;ma,|`8*:|&*91');
define('SECURE_AUTH_SALT', 'eP,n|_f5ukzJCeY4k!s(,.p2AReG^$8/=g)EmBh0(lH*0~7Q`]=v|z&w4CqgW0dc');
define('LOGGED_IN_SALT',   'q)?_O6>|m~uFl,r9>{W[8qOSz,B1sd0cK9!.c~Uj_#_,-`pC,nba|7!T5X*n^a(S');
define('NONCE_SALT',       'BN+p_Lcg=-XdeuaXm+~-Qmgn`{kBul8AtT*IMN.<88^uV69*w5E,Kd>$mnEwu|g=');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
