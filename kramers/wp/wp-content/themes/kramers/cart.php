<?php

// Template Name:Cart-Page

get_header();
?>
        <main>
            <div class="cart-page w-75 mx-auto">
                <h1 class="title text-center text-md-left text-lg-left wow fadeIn" data-wow-duration="1.5s" data-wow-delay=".3s">YOUR CART</h1>
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-6 mb-5 mb-md-5 mb-lg-0 text-center text-md-left text-lg-left wow fadeInLeft" data-wow-duration="1.5s"
                        data-wow-delay=".3s">
                        <?php
                            $args = array(
                                'post_type' => 'design_portfolio',
                                'order' => 'ASC',
                                'posts_per_page' => -1
                            );
                            $loop = new WP_Query($args);
                            while ($loop->have_posts()) : $loop->the_post();
                        ?>
                        <div class="cart-detail mb-5">
                            <div class="row">
                                <div class="col-sm-6 mb-5 mb-md-0 mb-lg-0">
                                    <div class="product-image mw-100 mx-auto">
                                        <img class="img-fluid wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s" src="assets/dist/images/product-1.png" alt="#">
                                    </div>
                                </div>
                                <div class="col-sm-6 d-md-flex d-lg-flex align-items-center">
                                    <ul>
                                        <li class="product-name wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                                            SOUP MIX
                                        </li>
                                        <li class="product-price wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                                            $6.00
                                        </li>
                                        <li class="product-style wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                                            style:
                                            <span class="ml-3 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">SPICY TORTILLA</span>
                                        </li>
                                        <li class="product-qt wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                                            qt:
                                            <span class="ml-3">1</span>
                                        </li>
                                        <li class="producttotal wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                                            total:
                                            <span class="ml-3">$6.00</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <?php
                            endwhile;
                        ?>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6 d-flex justify-content-around align-items-center wow fadeInRight" data-wow-duration="1.5s"
                        data-wow-delay=".3s">
                        <div class="check-out w-75 mx-auto p-5 position-relative">
                            <ul>
                                <li class="d-flex justify-content-between mb-3 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                                    <span>price:</span>
                                    <p> $30.00</p>
                                </li>
                                <li class="d-flex justify-content-between mb-3 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                                    <span>Tax:</span>
                                    <p class="text-left"> $0.00</p>
                                </li>
                                <li class="d-flex justify-content-between mb-3 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                                    <span>Shipping: </span>
                                    <p> $0.00</p>
                                </li>
                                <li class="d-flex justify-content-between mb-3 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                                    <span>TOTAL: </span>
                                    <p>$30.00</p>
                                </li>
                            </ul>
                            <div class="checkout-button w-100 position-absolute text-center wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                                <a class="text-uppercase" href="#">check out</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </main>
     <?php get_footer();