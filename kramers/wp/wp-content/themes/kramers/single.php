<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
?>
<?php
while (have_posts()) : the_post();
?>
<main>
    <div class="recipe-page w-75 mx-auto">
        <h1 class="title wow fadeIn text-uppercase" data-wow-duration="1.5s" data-wow-delay=".3s"><?php the_title(); ?></h1>
        <?php $price = get_post_meta(get_the_ID(), 'ms_price', true);
         ?>
        <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-6 order-2 order-md-2 order-lg-2 wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".3s">
                    <?php if($price!="") { ?>
                        <span class="product-price d-block mb-3 wow fadeIn" data-wow-duration="1.5s" data-wow-delay=".3s"><?php echo $price ?></span>
                    <?php } ?>
                    <div class="recipe-ingredients">
                        <?php the_content(); ?>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6 mb-5 mb-md-5 mb-lg-0 order-1 order-md-1 order-lg-2 wow fadeInRight" data-wow-duration="1.5s"data-wow-delay=".3s">
                    <div class="recipe-image wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                        <img src="<?php the_post_thumbnail_url(); ?>"alt="#">
                    </div>
                </div>
            </div>
    </div>
</main>
<?php
endwhile;
?>
<?php
get_footer();
?>
