<?php
// Template Name:Recipes-Template

get_header();
?>
    <div class="recipes">
                <div class="heading text-center shape position-relative text-uppercase wow fadeIn" data-wow-duration="1.5s" data-wow-delay=".3s">
                    <span class="wow flipInX" data-wow-duration="1.5s" data-wow-delay=".3s">
                        All recipes
                    </span>
                </div>
                <div class="w-75 mx-auto">
                    <div class="row">
                    <?php
                        $args = array(
                        'post_type' => 'recipe',
                        'order' => 'DESC',
                        'posts_per_page' => -1
                        );
                        $loop = new WP_Query($args);
                        while ($loop->have_posts()) : $loop->the_post();
                        ?>
                            <div class="col-sm-12 col-md-6 col-lg-3 text-center mb-5 wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".3s">
                                <div class="product-detail">
                                <div class="image wowo fadeIn" data-wow-duration="1.5s" data-wow-delay=".3s">
                                    <img src="<?php the_post_thumbnail_url(); ?>" alt="#">
                                </div>
                                <span class="title d-block text-uppercase wow fadeIn" data-wow-duration="1.5s" data-wow-delay=".3s"><?php the_title(); ?></span>
                                <div class="short-detail mx-auto text-uppercase">
                                    <?php the_excerpt(); ?>
                                </div>
                                <a class="submit-button text-uppercase wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s" href="<?php the_permalink(); ?>">view</a>
                                </div>
                        </div>
                        <?php
                            endwhile;
                        ?>
                    </div>
                </div>
            </div>
    
<?php get_footer();