<?php

/**
 * Initialize the options before anything else. 
 */
add_action('admin_init', 'custom_theme_options', 1);

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {
    /**
     * Get a copy of the saved settings array. 
     */
    $saved_settings = get_option('option_tree_settings', array());

    /**
     * Custom settings array that will eventually be 
     * passes to the OptionTree Settings API Class.
     */
    $custom_settings = array(
        'contextual_help' => array(
            'content' => array(
                array(
                    'id' => 'general_help',
                    'title' => 'General1',
                    'content' => '<p>Help content goes here!</p>'
                )
            ),
            'sidebar' => '<p>Sidebar content goes here!</p>',
        ),
        'sections' => array(
            array(
                'id' => 'header',
                'title' => 'Header Settings'
            ),
            array(
                'id' => 'social',
                'title' => 'Social Links'
            ),
             array(
                'id' => 'contact',
                'title' => 'Contact Setting'
            ),
            array(
                'id' => 'footersetting',
                'title' => 'Footer Settings'
            ),
        ),
        'settings' => array(
            array(
                'id' => 'logo_text',
                'label' => 'Logo Text',
                'desc' => '',
                'std' => '',
                'section' => 'header',
                'type' => 'text',
                'class' => '',
                'choices' => array()
            ),
            array(
                'id' => 'logo_text_2',
                'label' => 'Logo Text-2',
                'desc' => '',
                'std' => '',
                'section' => 'header',
                'type' => 'text',
                'class' => '',
                'choices' => array()
            ),
            array(
                'id' => 'toll_free',
                'label' => 'Operator',
                'desc' => '',
                'std' => '',
                'section' => 'contact',
                'type' => 'text',
                'class' => '',
                'choices' => array()
            ),
            array(
                'id' => 'toll_free_number',
                'label' => 'Contact Number',
                'desc' => '',
                'std' => '',
                'section' => 'contact',
                'type' => 'text',
                'class' => '',
                'choices' => array()
            ),
            array(
                'id' => 'email_to',
                'label' => 'Email To',
                'desc' => '',
                'std' => '',
                'section' => 'contact',
                'type' => 'text',
                'class' => '',
                'choices' => array()
            ),
            array(
                'id' => 'email',
                'label' => 'Email Address',
                'desc' => '',
                'std' => '',
                'section' => 'contact',
                'type' => 'text',
                'class' => '',
                'choices' => array()
            ),
            array(
                'id' => 'address',
                'label' => 'Address',
                'desc' => '',
                'std' => '',
                'section' => 'contact',
                'type' => 'text',
                'class' => '',
                'choices' => array()
            ),
            array(
                'id' => 'toll_free_number_2',
                'label' => 'Contact Number-2',
                'desc' => '',
                'std' => '',
                'section' => 'contact',
                'type' => 'text',
                'class' => '',
                'choices' => array()
            ),
            array(
                'id' => 'distributor',
                'label' => 'Distributor',
                'desc' => '',
                'std' => '',
                'section' => 'contact',
                'type' => 'text',
                'class' => '',
                'choices' => array()
            ),
            array(
                'id' => 'copyright',
                'label' => 'Copyright',
                'desc' => 'Enter Text Here.',
                'std' => '',
                'section' => 'footersetting',
                'type' => 'text',
                'class' => '',
                'choices' => array()
            ),
            array(
                'id' => 'social_facebook',
                'label' => 'Facebook URL',
                'desc' => 'Enter Linkedin link here.',
                'std' => '',
                'section' => 'social',
                'type' => 'text',
                'class' => '',
                'choices' => array()
            ),
        )
    );

    /* settings are not the same update the DB */
    if ($saved_settings !== $custom_settings) {
        update_option('option_tree_settings', $custom_settings);
    }
}

?>