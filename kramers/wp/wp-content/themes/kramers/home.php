<?php

    // Template Name:Home-Template

    get_header();
?>

        <!-- content -->
        <main>

            <!-- main detail -->
            <div class="main-detail">
                <div class="w-75 mx-auto">
                    <div class="row">
                        <div class="col-md-12 col-lg-4 wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".3s">
                            <span class="mb-10"><?php echo get_the_title('1'); ?></span>
                            <p><?php echo get_post_field('post_content', $post_1); ?></p>
                        </div>
                        <div class="col-md-12 col-lg-4 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                            <span><?php echo get_the_title('18')?></span>
                            <p><?php echo get_post_field('post_content',$post_18); ?></p>
                        </div>
                        <div class="col-md-12 col-lg-4 wow fadeInRight" data-wow-duration="1.5s" data-wow-delay=".3s">
                            <span><?php echo get_the_title('20'); ?></span>
                            <p><?php echo get_post_field('post_content',$post_20); ?></p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- product -->
            <div class="products">
                <div class="heading text-center shape position-relative text-uppercase wow fadeIn" data-wow-duration="1.5s">
                    <span class="wow fadeInUp" data-wow-duration="1.5s">
                        products
                    </span>
                </div>
                <div class="container-fluid">
                    <div class="row">
                    <?php
                        $args = array(
                        'post_type' => 'product',
                        'order' => 'DESC',
                        'posts_per_page' => 4
                        );
                        $loop = new WP_Query($args);
                        while ($loop->have_posts()) : $loop->the_post();
                        $price = get_post_meta(get_the_ID(), 'ms_price', true);
                    ?>
                        <div class="col-sm-12 col-md-6 col-lg-3 mb-3 text-center wow fadeInLeft" data-wow-duration="1.5s">
                            <div class="product-detail">
                                <?php the_ID; ?>
                                <img class="img-fluid mh-50 w-50 wow fadeIn" data-wow-duration="1.5s"  src="<?php the_post_thumbnail_url(); ?> ?>"
                                    alt="#">
                                <span class="title d-block text-uppercase wow fadeIn" data-wow-duration="1.5s" ><?php the_title(); ?></span>
                                <span class="price d-block wow fadeIn" data-wow-duration="1.5s" ><?php echo $price; ?></span>
                               <div class="short-detail mx-auto text-uppercase">
                                    <?php the_excerpt(); ?>
                               </div>
                                <a class="submit-button text-uppercase wow fadeInUp" data-wow-duration="1.5s"  href="<?php the_permalink(); ?>">Read More</a>
                            </div>
                        </div>
                    <?php endwhile;
                    ?>
                    </div>
                    <div class="row d-flex justify-content-center mt-5">
                        <div class="col-sm-3 text-center text-uppercase">
                            <a class="view-all" href="<?php bloginfo('url'); ?>/products/">All Products</a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- recipes -->
            <div class="recipes">
                <div class="heading text-center shape position-relative text-uppercase wow fadeIn" data-wow-duration="1.5s">
                    <span class="wow fadeInUp" data-wow-duration="1.5s" >
                        recipes
                    </span>
                </div>
                <div class="w-75 mx-auto">
                    <div class="row">
                    <?php
                        $args = array(
                        'post_type' => 'recipe',
                        'order' => 'DESC',
                        'posts_per_page' => 4
                        );
                        $loop = new WP_Query($args);
                        while ($loop->have_posts()) : $loop->the_post();
                        ?>
                            <div class="col-sm-12 col-md-6 col-lg-3 text-center mb-5 wow fadeInLeft" data-wow-duration="1.5s">
                                <div class="product-detail">
                                <div class="image wowo fadeIn" data-wow-duration="1.5s" >
                                    <img src="<?php the_post_thumbnail_url(); ?>" alt="#">
                                </div>
                                <span class="title d-block text-uppercase wow fadeIn" data-wow-duration="1.5s" ><?php the_title(); ?></span>
                                <div class="short-detail mx-auto text-uppercase">
                                    <?php the_excerpt(); ?>
                                </div>
                                <a class="submit-button text-uppercase wow fadeInUp" data-wow-duration="1.5s"  href="<?php the_permalink(); ?>">view</a>
                                </div>
                        </div>
                        <?php
                            endwhile;
                        ?>
                    </div>
                    <div class="row d-flex justify-content-center mt-5">
                        <div class="col-sm-3 text-center text-uppercase">
                            <a class="view-all" href="<?php bloginfo('url'); ?>/recipes/">All Recipes</a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- contan us -->
            <div class="contact-us">
                <div class="heading text-center shape position-relative text-uppercase wow fadeIn" data-wow-duration="1.5s">
                    <span class="wow bounceIn" data-wow-duration="1.5s" >
                        contact us
                    </span>
                </div>
                <div class="w-75 mx-auto">
                    <div class="row">
                        <div class="col-md-6 mb-5 mb-md-0 mb-lg-0 wow fadeInLeft" data-wow-duration="1.5s" ">
                        <?php echo do_shortcode( '[contact-form-7 id="1234" title="Contact form 1"]' ); ?>
                        </div>
                        <div class="col-md-6 contact-detail px-lg-5 px-md-5 wow fadeInRight" data-wow-duration="1.5s" ">
                            
                            <span class="email-title d-block text-capitalize wow fadeIn" data-wow-duration="1.5s" ><?php echo get_option_tree('email_to'); ?></span>
                            <a class="email d-block text-capitalize wow fadeIn" data-wow-duration="1.5s"  href="mailto:<?php echo get_option_tree('email'); ?>">email:<?php echo get_option_tree('email'); ?></a>
                            <span class="address d-block text-capitalize wow fadeIn" data-wow-duration="1.5s" ><?php echo get_option_tree('address'); ?></span>
                            <span class="phone d-block text-capitalize wow fadeIn" data-wow-duration="1.5s" ><?php echo get_option_tree('toll_free_number'); ?></span>
                            <span class="phone-2 d-block text-capitalize wow fadeIn" data-wow-duration="1.5s" ><?php echo get_option_tree('toll_free_number_2'); ?></span>
                            <span class="distributor d-block text-capitalize wow fadeIn" data-wow-duration="1.5s" ><?php echo get_option_tree('distributor'); ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </main>

      <?php get_footer();