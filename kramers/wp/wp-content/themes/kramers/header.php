<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/dist/css/bootstrap-min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/dist/css/style-min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/dist/css/animate-min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css">
    <title>kramers</title>
    <?php wp_head(); ?>
</head>
    <body <?php body_class(); ?>>
       
    <div id="wrapper">

<!-- header -->
    <header>
    <div class="container-fluid">
        <div class="header py-5">
            <div class="row">
                <div class="col-md-4 col-lg-4"></div>
                <div class="col-sm-12 col-md-4 col-lg-4 mb-5 mb-md-0 mb-lg-0">
                    <div class="logo text-center wow fadeInDown" data-wow-duration="1.5s" data-wow-delay=".3s" data-wow-duration="2s" data-wow-delay=".3s">
                        <a href="<?php echo site_url(); ?>">
                            <span class="kramers text-capitalize d-block"><?php echo get_option_tree('logo_text'); ?></span>
                            <span class="salsa text-uppercase"><?php echo get_option_tree('logo_text_2'); ?></span>
                        </a>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 cart text-center wow fadeIn" data-wow-duration="2s" data-wow-delay=".3s">
                    <p class="text-center text-uppercase m-0 wow fadeInRight" data-wow-duration="1.5s" data-wow-delay="1s"><?php echo get_option_tree('toll_free'); ?></p>
                    <p class="text-center  text-uppercase wow fadeInRight" data-wow-duration="1.5s" data-wow-delay="1s"><?php echo get_option_tree('toll_free_number'); ?> </p>
                    <ul>
                        <li class="d-inline-block facebook wow fadeInRight" data-wow-duration="1.5s" data-wow-delay=".3s">
                            <a href="<?php echo get_option_tree('social_facebook'); ?>" target="blank">
                                <i class="fab fa-facebook-f "></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>