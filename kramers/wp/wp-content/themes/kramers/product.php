<?php
// Template Name:Produt-Template

get_header();
?>
      
      <div class="products">
                <div class="heading text-center shape position-relative text-uppercase wow fadeIn" data-wow-duration="1.5s" data-wow-delay=".3s">
                    <span class="wow flipInX" data-wow-duration="1.5s" data-wow-delay=".3s">
                       All products
                    </span>
                </div>
                <div class="container-fluid">
                    <div class="row">
                    <?php
                        $args = array(
                        'post_type' => 'product',
                        'order' => 'DESC',
                        'posts_per_page' => -1
                        );
                        $loop = new WP_Query($args);
                        while ($loop->have_posts()) : $loop->the_post();
                        $price = get_post_meta(get_the_ID(), 'ms_price', true);
                    ?>
                        <div class="col-sm-12 col-md-6 col-lg-3 mb-3 text-center wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".3s">
                            <div class="product-detail">
                                <img class="img-fluid mh-50 w-50 wow fadeIn" data-wow-duration="1.5s" data-wow-delay=".3s" src="<?php the_post_thumbnail_url(); ?> ?>"
                                    alt="#">
                                <span class="title d-block text-uppercase wow fadeIn" data-wow-duration="1.5s" data-wow-delay=".3s"><?php the_title(); ?></span>
                                <span class="price d-block wow fadeIn" data-wow-duration="1.5s" data-wow-delay=".3s"><?php echo $price; ?></span>
                               <div class="short-detail mx-auto text-uppercase">
                                    <?php the_excerpt(); ?>
                               </div>
                                <a class="submit-button text-uppercase wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s" href="<?php the_permalink(); ?>">Read More</a>
                            </div>
                        </div>
                    <?php endwhile;
                    ?>
                    </div>
                </div>
            </div>


        
        <?php get_footer();