<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */
?>

  <!-- footer -->
  <footer class="wow fadeIn" data-wow-duration="1.5s" data-wow-delay=".5s">
            <span class="d-block text-center text-uppercase wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="1s"><?php echo get_option_tree('copyright'); ?></span>
        </footer>
    </div>

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

    <script src="<?php bloginfo('template_directory')?>/assets/dist/js/merge.js"></script>
    <script>
        new WOW().init();
    </script>
    <?php get_footer(); ?>
</body>

</html>
