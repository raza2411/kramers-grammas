<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <?php wp_head(); ?>
    </head>

    <body>

        <header>
            <div class="container">
                <div class="row">
                    <div class="col-12 text-right">
                        <span class="fa-icons"><?php echo ot_get_option('phone_number'); ?></span><br>
                        <div class="pr-3">
                            <a href="<?php echo get_option_tree('fb_link'); ?>" target="blank">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facebook.png" class="img-fluid cart" alt="social-media">
							</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 offset-md-3 pb-4 col-8 offset-2">
                        <a href="our-story">
                            <img src="<?php echo ot_get_option('other_pages_logo'); ?>" alt="grammas" class="img-fluid">
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <nav class="navbar navbar-expand-lg py-3 px-5 nav-justified black-text">
                            <button class="navbar-toggler border" type="button" data-toggle="collapse" data-target="#navbars" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="fa fa-bars"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="navbars">
                                <?php
                                wp_nav_menu(array
                                    (
                                    'menu' => 'Primary Menu',
                                    'container' => '',
                                    'container_class' => '',
                                    'container_id' => '',
                                    'menu_class' => 'navbar-nav align-items-center w-100',
                                    'menu_id' => '',
                                    'link_before' => '<span class="border-style px-3 pt-3 pb-2 dark">',
                                    'link_after' => '</span'
                                    . '>'
                                ));
                                ?>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </header>