<?php

/**
 * Template Name: page-contact
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
?>

<section class="" id="contact">
    <div class="container">
        <div class="row mt-md-5">
            <div class="col-md-8 mt-md-5 mt--2 offset-md-2 px-5">
                <div>
                    
                    <?php echo do_shortcode('[contact-form-7 id="56" title="Contact form 1"]')?>
                    
<!--                    <form action ="contact-us.html" method ="post">
                        <input type="text" name="name" placeholder="NAME:" class="form-input w-100 border-style my-2 py-2 pl-2">
                        <input type="text" name="business" placeholder="BUSINESS:" class="form-input w-100 border-style my-2 py-2 pl-2">
                        <input type="text" name="phone" placeholder="PHONE" class="form-input w-100 border-style my-2 py-2 pl-2">
                        <input type="text" name="address" placeholder="ADDRESS" class="form-input w-100 border-style my-2 py-2 pl-2">
                        <textarea class="py-2 pl-2 w-100 my-2 border-style" placeholder="COMMENTS" rows="7"></textarea>
                        <div class="submit text-center my-3">
                            <input type="submit" value="SUBMIT" name="submit" class="darker border-style px-3 py-2 border-0">
                        </div>

                    </form>-->
                    
                </div>
            </div>
        </div>
    </div>
</section>

<?php

get_footer();
