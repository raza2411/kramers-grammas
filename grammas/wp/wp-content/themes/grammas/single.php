<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<!-- 
<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main"> -->

			<?php
			/* Start the Loop */
			// while ( have_posts() ) : the_post();

			// 	get_template_part( 'template-parts/post/content', get_post_format() );

			// 	// If comments are open or we have at least one comment, load up the comment template.
			// 	if ( comments_open() || get_comments_number() ) :
			// 		comments_template();
			// 	endif;

			// 	the_post_navigation( array(
			// 		'prev_text' => '<span class="screen-reader-text">' . __( 'Previous Post', 'twentyseventeen' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Previous', 'twentyseventeen' ) . '</span> <span class="nav-title"><span class="nav-title-icon-wrapper">' . twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '</span>%title</span>',
			// 		'next_text' => '<span class="screen-reader-text">' . __( 'Next Post', 'twentyseventeen' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Next', 'twentyseventeen' ) . '</span> <span class="nav-title">%title<span class="nav-title-icon-wrapper">' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ) . '</span></span>',
			// 	) );

			// endwhile; // End of the loop.
			?>

<!-- 		</main> --><!-- #main -->
<!-- 	</div> --><!-- #primary -->
 <!-- 	<?php// get_sidebar(); ?> -->
<!-- </div> --><!-- .wrap -->
<?php
				while (have_posts()) : the_post();
                    $price = get_post_meta(get_the_ID(), 'ms_price', true);
			?>
<section class="" id="story">
    <div class="container">
        <div class="row mt-5">

            <div class="col-md-6 mt-5 wow fadeInLeft" data-wow-duration="2s">

                <?php $post = get_post(get_the_ID()); ?>
                <h2><?php echo $post->post_title; ?></h2>
                <h5 class="mb-4"><span><?php echo $price; ?></span></h5>

                <?php echo $post->post_content; ?>
            </div>

            <div class="col-md-6 mt-4 circle-main wow fadeInLeft" data-wow-duration="2s">
                        <div class="product-photo-circle my-5 text-center">
                            <?php the_post_thumbnail('url'); ?>
                        </div>
                    </div>
        </div>
    </div>
</section>
<?php
				endwhile;
				?>

<?php get_footer();
