<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */
?>

<script>
jQuery(document).ready(function() {
   
   jQuery("p").addClass("aos-init aos-animate");
   jQuery("#story h2").addClass("aos-init aos-animate");
});

</script>
<!-- jquery -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery-3.2.1.min.js"></script>
<!-- proper-js -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/popper.min.js"></script>
<!-- bootstrap -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/wow.js"></script>
    <script>
        new WOW().init();
    </script>


</body>
</html>
