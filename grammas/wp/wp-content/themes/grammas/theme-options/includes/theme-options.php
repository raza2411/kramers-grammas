<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Initialize the options before anything else. 
 */
add_action('init', 'custom_theme_options', 1);

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {

    /* OptionTree is not loaded yet, or this is not an admin request */
    if (!function_exists('ot_settings_id') || !is_admin())
        return false;

    /**
     * Get a copy of the saved settings array. 
     */
    $saved_settings = get_option('option_tree_settings', array());

    /**
     * Custom settings array that will eventually be 
     * passes to the OptionTree Settings API Class.
     */
    $custom_settings = array(
        'sections' => array(
            array(
                'id' => 'header_settings',
                'title' => 'Primary Logo'
            ),
            array(
                'id' => 'other_logo',
                'title' => 'Secondary Logo'
            ),
            array(
                'id' => 'phone_number',
                'title' => 'Phone'
            ),
            array(
                'id' => 'fb_link',
                'title' => 'Facebook Link'
            ),
        ),
        // General Settings
        'settings' => array(
            //Header Settings
            array(
                'id' => 'header_logo',
                'label' => 'Select Logo',
                'desc' => '',
                'std' => '',
                'type' => 'Upload',
                'section' => 'header_settings',
                'class' => '',
                'choices' => array(
                    'value' => 'Yes',
                    'label' => 'Yes',
                ),
            ),
            array(
                'id' => 'other_pages_logo',
                'label' => 'Select Logo',
                'desc' => '',
                'std' => '',
                'type' => 'Upload',
                'section' => 'other_logo',
                'class' => '',
                'choices' => array(
                    'value' => 'Yes',
                    'label' => 'Yes',
                ),
            ),
            array(
                'id' => 'phone_number',
                'label' => 'PHONE',
                'desc' => '',
                'std' => '',
                'type' => 'text',
                'section' => 'phone_number',
                'class' => '',
                'choices' => array(
                    'value' => 'Yes',
                    'label' => 'Yes',
                ),
            ),
            array(
                'id' => 'fb_link',
                'label' => 'Facebook Link',
                'desc' => '',
                'std' => '',
                'type' => 'text',
                'section' => 'fb_link',
                'class' => '',
                'choices' => array(
                    'value' => 'Yes',
                    'label' => 'Yes',
                ),
            ),
        )
    );


    /* settings are not the same update the DB */
    if ($saved_settings !== $custom_settings) {
        update_option('option_tree_settings', $custom_settings);
    }

    /* Lets OptionTree know the UI Builder is being overridden */
    global $ot_has_custom_theme_options;
    $ot_has_custom_theme_options = true;
}
