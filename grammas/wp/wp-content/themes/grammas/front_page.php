<?php
/**
 * Template Name: Blank Template With Slider
 * Template Post Type: post, page
 * The template for displaying RevSlider on a blank page
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
	<style type="text/css">
		body:before { display:none !important}
		body:after { display:none !important}
		body { background:transparent}
	</style>
</head>

<body <?php body_class(); ?>>
<div>
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post(); ?>

			<!--// Include the page content template.-->
			<!--//echo do_shortcode( get_the_content() );-->
<?php echo do_shortcode('[rev_slider alias="slider3"]'); ?>
        <section class="" id="index">
            <div class="container">
                <div class="row">
                    <div class="w-100 absolute">
                        <div class="click relative wow flipInY" data-wow-duration="3s">
                            <div class="ripple-out">
                                <a href="<?php echo site_url(); ?>/our-story" class="ripple-out-a p-5">Click <br>To<br> Enter</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>			
			
<?php
		// End the loop.
		endwhile;
		?>
</div>
<?php wp_footer(); ?>

</body>
</html>

