<?php

/**
 * Template Name: page-products
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
?>

<section class="pb-5 mb-5" id="products">
    <div class="container">
        <div class="row">
            <?php
                        $args = array(
                        'post_type' => 'product',
                        'order' => 'ASC',
                        'posts_per_page' => -1
                        );
                        $loop = new WP_Query($args);
                        while ($loop->have_posts()) : $loop->the_post();
                        $price = get_post_meta(get_the_ID(), 'ms_price', true);
                    ?>
            <div class="col-md-3 col-sm-6 col-12 mt-4 circle-main wow fadeInLeft" data-wow-duration="2s">
                <div class="product-photo-circle mt-5 text-center">
                    <?php the_post_thumbnail('url'); ?>
                </div>
                <div class="text-center mt-5">
                    <h5><?php the_title(); ?></h5>
                    <h5 class="mb-4"><span><?php echo $price; ?></span></h5>
                    <p class="text-justify px-3 pt-1">
                        <?php the_excerpt(); ?>
                        <a href="<?php the_permalink(); ?>">Read more</a>
                    </p>
                </div>
            </div>
                <?php endwhile;
                ?>
            

        </div>
    </div>
</section>

<?php

get_footer();
