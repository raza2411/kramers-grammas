<?php

/**
 * Template Name: page-home
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
?>

<section class="pb-5 mb-5" id="cart">
    <div class="container">
        <div class="row">
            <div class="col-md-3 mt-4 col-sm-4 wow fadeInLeft" data-wow-duration="2s">
                <div class="product-photo-circle mt-5">
                    <span class="product-photo" >
                        PRODUCT PHOTO
                    </span>
                </div>
            </div>
            <div class="col-md-5 mt-sm-5 mt-4 col-sm-8 wow fadeInLeft" data-wow-duration="2s">
                <div class="mt-sm-5 salad">
                    <h2>SALAD DRESSING</h2>
                    <h2><span>$6.00</span></h2>
                    <h2><span>QT:1</span></h2>
                    <h2>TOTAL: <span>$6.00</span></h2>
                </div>
            </div>
            <div class="col-md-4 mt-sm-5 wow fadeInRight" data-wow-duration="2s">
                <div class="check mt-sm-5 mt-4 relative">
                    <div class="px-5 py-5 dark border-style">
                        <table>
                            <tr>
                                <td class="pr-4"><h3>Price:</h3></td>
                                <td><h3>$30.00</h3></td>
                            </tr>
                            <tr>
                                <td class="pr-4"><h3>Tax:</h3></td>
                                <td><h3>$0.00</h3></td>
                            </tr>
                            <tr>
                                <td class="pr-4"><h3>Shipping</h3></td>
                                <td><h3>$0.00</h3></td>
                            </tr>
                            <tr>
                                <td class="pr-4"><h4>TOTAL</h4></td>
                                <td><h4>$30.00</h4></td>
                            </tr>

                        </table>
                    </div>
<!--                     <div class="w-100 check-out">
                        <span class="px-4 py-3 darker border-style">CHECK OUT</span>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</section>

<?php

get_footer();
