<?php
/**
 * Template Name: page-main
 *

 */
wp_head();
?>
<?php
		// Start the loop.
		while ( have_posts() ) : the_post(); ?>

			<!--// Include the page content template.-->
			<!--//echo do_shortcode( get_the_content() );-->
<?php echo do_shortcode('[rev_slider alias="slider3"]'); ?>
<div class="overlay">
	
</div>
    <header>
        <div class="container">
            <div class="row mt-3">
                <div class="col-12 text-right text-white">
                    <span class="fa-icons">641-590-1503</span><br>
                    <div class="pr-3">
                        <a href="<?php echo get_option_tree('fb_link'); ?>" target="blank">
                            <img src="<?php bloginfo('template_directory')?>/assets/images/facebook-2.png" class="img-fluid cart">
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 offset-md-3 col-8 offset-2 pb-4 w-100 w-md-75 text-center">
                    <a href="<?php bloginfo('url'); ?>/our-story">
                        <img src="<?php echo ot_get_option('header_logo'); ?>" alt="grammas" class="logo-img img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </header>
<section class="" id="index">
    <div class="container">
        <div class="row mt-5">
            <div class="w-100 absolute">
                <div class="click relative wow flipInY" data-wow-duration="3s">
                    <div class="ripple-out">
                        <a href="<?php bloginfo('url'); ?>/our-story" class="ripple-out-a p-5">Click To Enter</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
		// End the loop.
		endwhile;
		?>
		<?php
        wp_footer();