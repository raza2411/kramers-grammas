<?php
/**
 * Template Name: page-story
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
?>

<section class="" id="story">
    <div class="container">
        <div class="row mt-5">

            <div class="col-md-6 wow fadeInLeft" data-wow-duration="2s">

                <?php $post = get_post(get_the_ID()); ?>
                <h2><?php echo $post->post_title; ?></h2>

                <?php echo $post->post_content; ?>
            </div>

            <div class="col-md-6 wow fadeInRight" data-wow-duration="2s">
                <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="Read Our Story" class="img-fluid">
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
